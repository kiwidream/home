import React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";

import { translate } from "./js/translate";
import { setLocale } from "./js/translate";
import { CONSTANTS } from "./js/constants";

import headerImg from "./img/lockup.svg"

import Overview from "./components/Overview"
// import News from "./components/news"
// import API from "./components/api"

// import firebase from "firebase/app";

// // Add the Firebase services that you want to use
// import "firebase/auth";
// import "firebase/firestore";

// const firebaseConfig = {
//     apiKey: "AIzaSyDR8762X1sww0n9yyoKklutUAV7qsOAK-U",
//     authDomain: "test-3a7af.firebaseapp.com",
//     databaseURL: "https://test-3a7af.firebaseio.com",
//     projectId: "test-3a7af",
//     storageBucket: "test-3a7af.appspot.com",
//     messagingSenderId: "801396039695",
//     appId: "1:801396039695:web:b689f124758e2d73"
//   };

// firebase.initializeApp(firebaseConfig);

// var db = firebase.firestore();

class App extends React.Component {

    constructor(props) {
        super(props);

        this.enLang = CONSTANTS.enLang;
        this.zhLang = CONSTANTS.zhLang;

        this.lang = navigator.language === this.zhLang ? this.zhLang : this.enLang;

        this.state = { lang: this.lang, user: {} };

        this.divRef = null;
        this.btnRef = null;

        this.data = [];
    }

    componentDidMount() {
        // firebase.auth().onAuthStateChanged((user) => {
        //     if (user) {
        //         console.log("---------auth refresh on AuthStageChange---------")
        //         this.setState({ user })
        //     }
        // })

        // db.collection("dayUpdate2021").get().then((querySnapshot) => {
        //     querySnapshot.forEach((doc) => {
        //         // this.data.push(doc.data())
        //         this.data.push(doc.data())
        //     });
        // });
    }

    hideMenu() {
        // console.log("-------hideMenu-----------")
        this.divRef.className = this.divRef.className.replace("show", "");
        this.btnRef.blur();
    }

    // signin() {
    //     console.log("---------signin---------")
    //     let currentUser = firebase.auth().currentUser;
    //     if (currentUser && !currentUser.isAnonymous) {
    //         console.log("---------user already login---------")
    //         console.log(currentUser.uid)

    //     } else {
    //         console.log("---------user begin login---------")
    //         var provider = new firebase.auth.GoogleAuthProvider();
    //         firebase.auth().signInWithRedirect(provider);
    //         currentUser = firebase.auth().currentUser;
    //     }

    //     console.log(currentUser.displayName)
    //     this.setState({ user: currentUser })
    // }

    // logout() {
    //     console.log("---------logout---------")
    //     firebase.auth().signOut().then(() => {
    //         this.setState({ user: {} })
    //     })
    // }

    switchLang() {
        console.log(`--------App switchLang:--------------`)
        this.setState(preState => ({ lang: preState.lang === this.enLang ? this.zhLang : this.enLang }))
    }

    // update(collection, selectedRow) {
    //     // Add a new document in collection "cases"
    //     console.log(`--------overview update: ${JSON.stringify(selectedRow)}--------------`)
    //     var db = firebase.firestore();
    //     let id = selectedRow.id;
    //     if (id === 'newzealand') {
    //         let temp = Object.assign({}, selectedRow);
    //         temp.id = selectedRow.date;
    //         delete temp.date;
    //         db.collection('more').doc(temp.id).set(temp)
    //             .then(function () {
    //                 console.log("Document successfully update!");
    //             })
    //             .catch(function (error) {
    //                 if (error.code === "permission-denied") {
    //                     alert(translate("error403"))
    //                 }
    //                 console.error("Error updating document: ", JSON.stringify(error));

    //             });
    //     } else {
    //         db.collection(collection).doc(id).set(selectedRow)
    //             .then(function () {
    //                 console.log("Document successfully update!");
    //             })
    //             .catch(function (error) {
    //                 if (error.code === "permission-denied") {
    //                     alert(translate("error403"))
    //                 }
    //                 console.error("Error updating document: ", JSON.stringify(error));

    //             });
    //     }
    // }

    // del(collection, selectedRow) {
    //     console.warn(`--------App del: ${selectedRow}--------------`)
    //     var db = firebase.firestore();
    //     db.collection(collection).doc(selectedRow.id).delete().then(function () {
    //         console.log("Document successfully deleted!");
    //     }).catch(function (error) {
    //         console.error("Error removing document: ", error);
    //     });
    // }

    render() {
        setLocale(this.state.lang);

        let contactID = this.state.lang === 'en-US' ? 'DQSIkWdsW0yxEjajBLZtrQAAAAAAAAAAAAZAAO1Ih49UMVJZT0MzVzNUVkIwTjJFSEI0WU1PTjgwRy4u' : 'DQSIkWdsW0yxEjajBLZtrQAAAAAAAAAAAAZAAO1Ih49UNFQzSDdFSFozSVNETU5OVzdaMVpSTUIwUy4u';

        return (
            <>
                <div className="container text-center" onScroll={(e) => this.hideMenu(e)}>
                    <Router basename={'./'}>
                        <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
                        <a className="navbar-brand" href="#/"><img height="40px" alt="brand" src={headerImg}></img></a>
                            <button ref={btnEle => this.btnRef = btnEle} className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>
                            <div className="collapse navbar-collapse" id="navbarCollapse" ref={divEle => this.divRef = divEle} onClick={(e) => this.hideMenu(e)}>
                                <ul className="navbar-nav mr-auto">
                                    <li className="nav-item">
                                        <Link className="nav-link" to="/" >{translate("home")}</Link>
                                    </li>
                                    {/* <li className="nav-item">
                                        <Link className="nav-link" to="/news">{translate("annoucement")}</Link>
                                    </li>
                                    <li className="nav-item">
                                        <Link className="nav-link" to="/api">{translate("api")}</Link>
                                    </li> */}
                                </ul>

                                

                                {/* {this.state.user.displayName ? <div  onClick={(e) => this.hideMenu(e)}><span className="my-2 mx-2 my-sm-0 text-light">{translate("hi")} {this.state.user.displayName}</span> <button className="btn btn-outline-light my-2 mx-2 my-sm-0" onClick={(e) => this.logout(e)}>{translate("logout")}</button></div> : <button className="btn btn-outline-secondary my-2 mx-2 my-sm-0" onClick={(e) => this.signin(e)}>{translate("signin")}</button>} */}

                                <button className="btn btn-outline-success my-2 my-sm-0 ml-2" onClick={(e) => this.switchLang(e)}>{translate("lang")}</button>
                            </div>
                        </nav>

                        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
                        <Switch>
                            {/* <Route path="/news">
                                <News hideMenu={(e) => this.hideMenu(e)} fb={firebase} update={(e) => this.update("news", e)} del={(e) => this.del("news", e)}></News>
                            </Route>
                            <Route path="/api">
                                <API hideMenu={(e) => this.hideMenu(e)} fb={firebase} update={(e) => this.update("apis", e)} del={(e) => this.del("apis", e)}></API>
                            </Route> */}
                            <Route path="/">
                                <Overview hideMenu={(e) => this.hideMenu(e)} update={(e) => this.update("cases", e)} del={(e) => this.del("cases", e)} lang={this.state.lang}></Overview>
                            </Route>
                        </Switch>
                    </Router>
                </div>

                <footer className="container py-5">
                    <div className="row">
                        <div className="col-12 col-md">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="d-block mb-2"><circle cx="12" cy="12" r="10"></circle><line x1="14.31" y1="8" x2="20.05" y2="17.94"></line><line x1="9.69" y1="8" x2="21.17" y2="8"></line><line x1="7.38" y1="12" x2="13.12" y2="2.06"></line><line x1="9.69" y1="16" x2="3.95" y2="6.06"></line><line x1="14.31" y1="16" x2="2.83" y2="16"></line><line x1="16.62" y1="12" x2="10.88" y2="21.94"></line></svg>
                            <small className="d-block mb-3 text-muted">© 2017- {new Date().getFullYear()} </small>
                        </div>
                        <div className="col-6 col-md">
                            <h5>{translate("features")}</h5>
                            <ul className="list-unstyled text-small">
                                <li><a className="text-muted" href="#/">{translate("autoUpdate")}</a></li>
                                {/* <li><a className="text-muted" href="#/">{translate("signinManage")}</a></li> */}
                                <li><a className="text-muted" href="#/">{translate("bilang")}</a></li>
                                <li><a className="text-muted" href="#/">{translate("mapView")}</a></li>
                                <li><a className="text-muted" href="#/">{translate("pwa")}</a></li>
                            </ul>
                        </div>
                        <div className="col-6 col-md">
                            <h5> {translate("resources")} </h5>
                            <ul className="list-unstyled text-small">
                                <li><a className="text-muted" href="https://us-central1-test-3a7af.cloudfunctions.net/dayUpdate2021">{translate("api1")}</a></li>
                                <li>&nbsp;</li>
                                <li><a className="text-muted" href="https://us-central1-test-3a7af.cloudfunctions.net/cases">{translate("api2")}</a></li>
                                <li>&nbsp;</li>
                                <li><a className="text-muted" href="https://us-central1-test-3a7af.cloudfunctions.net/imageUpdate">{translate("api3")}</a></li>
                            </ul>
                        </div>
                        <div className="col-6 col-md">
                            <h5> {translate("projects")}</h5>
                            <ul className="list-unstyled text-small">
                                <li><a className="text-muted" href="#/api">{translate("crawler")}</a></li>
                                <li>&nbsp;</li>
                                <li><a className="text-muted" href="#/api">{translate("api")}</a></li>
                                <li>&nbsp;</li>
                                <li><a className="text-muted" href={`https://forms.office.com/Pages/ResponsePage.aspx?id=${contactID}`}><span className="text-primary" >{translate("contact")}</span></a></li>
                            </ul>
                        </div>
                        <div className="col-6 col-md">
                            <h5> {translate("techStack")} </h5>
                            <ul className="list-unstyled text-small">
                                <li><a className="text-muted" href="https://gitlab.com/">Gitlab</a></li>
                                <li><a className="text-muted" href="https://firebase.google.com/">Firebase</a></li>
                                <li><a className="text-muted" href="https://reactjs.org/">React</a></li>
                                <li><a className="text-muted" href="https://www.echartsjs.com/en/index.html">Echarts</a></li>
                            </ul>
                        </div>
                    </div>
                </footer>
            </>
        );
    }

}

export default App;